# Othello AI

Using minmax and alpha-beta pruning with a set depth of 6

## Important points in this AI

- Heuristics
- Killer Moves
- End-game solver
- Transposition Table
- Move ordering

### Heuristics used :

- Piece Difference
- Corner occupancy
- Corner closeness
- Mobility
- Disc Squares

### Killer moves:

Corners

### End-game solver:

If its at the end phase of the game (lasts 14 moves), it will not use heuristics but instead use a simple piece count

### Move ordering :

Sorts the moves array in descending order, based on the value of the move in the board, this can provide a faster prun

## Things that didnt work

### Iterative Deepening

Due to js being single threaded, i wasnt able to implement a working iterative deepening algorithm though i tried.
 Workers were being block by the main thread's loops and the answers were returning after i needed them due to this.
 I looked at promises and saw it would have the same issue, as well as a timeout and await (since its basically a
 promise).

 Due to all this, the algorithm ended up with a static depth search of 5.

### Resources

#### Rules

http://www.britishothello.org.uk/rules.html

#### AI

https://en.wikipedia.org/wiki/Computer_Othello

https://en.wikipedia.org/wiki/Minimax

http://chessprogramming.wikispaces.com/Parallel+Search

https://chessprogramming.wikispaces.com/Young+Brothers+Wait+Concept

https://en.wikipedia.org/wiki/Load_balancing_%28computing%29

https://cs.stackexchange.com/questions/998/distributed-alpha-beta-pruning

http://www.mkorman.org/othello.pdf

https://pdfs.semanticscholar.org/presentation/c5ee/85d54c48ae849a5ec36bea7d2e0025e75c30.pdf

https://web.stanford.edu/class/cs221/2017/restricted/p-final/man4/final.pdf

http://samsoft.org.uk/reversi/openings.htm

http://ledpup.blogspot.com/search/label/Reversi

http://xenon.stanford.edu/%7Elswartz/cs221/desdemona_writeup.pdf

http://radagast.se/othello/howto.html

http://pressibus.org/ataxx/autre/minimax/node3.html

https://www.ocf.berkeley.edu/~yosenl/extras/alphabeta/alphabeta.html

#### Alpha Beta Pruning Explanation

http://web.cs.ucla.edu/~rosen/161/notes/alphabeta.html
