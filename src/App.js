import React, { Component } from 'react';
import './App.css';
import socketIOClient from 'socket.io-client'

class App extends Component {

  constructor(){
      super();

      this.state = {
          endpoint: "",
          username: "",
          tournament_id: 1,
          tileClicked: 1,
          socket: socketIOClient(null)

  };
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handleTnmtChange = this.handleTnmtChange.bind(this);
      this.handleIpChange = this.handleIpChange.bind(this);
  }

  handleNameChange = (event) => {
    this.setState({username: event.target.value})
  };

  handleTnmtChange = (event) => {
    this.setState({tournament_id: event.target.value})
    };

  handleIpChange = (event) => {
        let ip = event.target.value;
        this.setState({endpoint: ip, socket: socketIOClient(ip)})
    };

  signin  = () =>{
      this.state.socket.emit('signin', {
          user_name: this.state.username,
          tournament_id: this.state.tournament_id,
          user_role: 'player'
      });
  };

  // needs optimization
  getLegalMoves(boardTwoD, indices, player){
      let moves = [];
      let tmpIdx;
      let piece;
      let tmpIdy;
      let tmpPieces;
      let map = new Map();
      let oneDPos;
      for (let i = 0; i < indices.length; i ++){

          // to check legal move since im checking in all directions already, if i place a piece on top ,it doesnt
          // matter to check top side of it because ill check it on the bottom part of the if statement, so i need to
          // check only in the opposite direction in each if statement

          let x = indices[i][0];
          let y = indices[i][1];

          // check top
          if (x !== 7 && x !== 0 && boardTwoD[x - 1][y] === 0){
              tmpPieces = [[x,y]];
              tmpIdx = x + 1;
              piece = boardTwoD[tmpIdx][y];
              while (tmpIdx !== 8 && piece !== 0){
                  piece = boardTwoD[tmpIdx][y];
                  if(piece === player) {
                      oneDPos = ((x - 1) * 8 + y);
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x - 1, y]);
                      break;
                  }
                  else{
                      tmpPieces.push([tmpIdx, y])
                  }
                  tmpIdx ++;
              }
          }
          // check bottom
          if (x !== 7 && x !== 0 && boardTwoD[x + 1][y] === 0){

              tmpPieces = [[x,y]];
              tmpIdx = x - 1;
              piece = boardTwoD[tmpIdx][y];
              while (tmpIdx !== -1 && piece !== 0){
                  piece = boardTwoD[tmpIdx][y];
                  if(piece === player){
                      oneDPos = ((x + 1) * 8 + y);
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x + 1, y]);
                      break
                  }
                  else{
                      tmpPieces.push([tmpIdx, y])
                  }

                  tmpIdx --;
              }

          }

          //check left
          if (y !== 0 && y !== 7 && boardTwoD[x][y - 1] === 0){

              tmpPieces = [[x,y]];
              tmpIdx = y + 1;
              piece = boardTwoD[x][tmpIdx];
              while (tmpIdx !== 8 && piece !== 0) {
                  piece = boardTwoD[x][tmpIdx];
                  if (piece === player){
                      oneDPos = (x * 8 + (y - 1));
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x, y - 1]);
                      break
                  }
                  else{
                      tmpPieces.push([x, tmpIdx])
                  }

                  tmpIdx++;
              }

          }
          //check right
          if (y !== 0 && y !== 7 && boardTwoD[x][y + 1] === 0){

              tmpPieces = [[x,y]];
              tmpIdx = y - 1;
              piece = boardTwoD[x][tmpIdx];
              while (tmpIdx !== -1 && piece !== 0){
                  piece = boardTwoD[x][tmpIdx];
                  if(piece === player) {
                      oneDPos = (x * 8 + (y + 1));
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x, y + 1]);
                      break
                  }
                  else {
                      tmpPieces.push([x, tmpIdx])
                  }

                  tmpIdx --;

              }

          }
          //diagonal top left
          if (x !== 0 && y !== 0 && x !== 7 && y !== 7 && boardTwoD[x - 1][y - 1] === 0) {
              tmpPieces = [[x, y]];
              tmpIdx = x + 1;
              tmpIdy = y + 1;
              piece = boardTwoD[tmpIdx][tmpIdy];
              while (tmpIdx !== 8 && tmpIdy !== 8 && piece !== 0) {
                  piece = boardTwoD[tmpIdx][tmpIdy];
                  if (piece === player) {
                      oneDPos = ((x - 1) * 8 + (y - 1));
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x - 1, y - 1]);
                      break
                  }
                  else {
                      tmpPieces.push([tmpIdx, tmpIdy])
                  }

                  tmpIdx++;
                  tmpIdy++;

              }
          }
          //diagonal bottom left
          if (x !== 7 && y !== 0 && x !== 0 && y !== 7 && boardTwoD[x + 1][y - 1] === 0) {
              tmpPieces = [[x, y]];
              tmpIdx = x - 1;
              tmpIdy = y + 1;
              piece = boardTwoD[tmpIdx][tmpIdy];
              while (tmpIdx !== -1 && tmpIdy !== 8 && piece !== 0) {
                  piece = boardTwoD[tmpIdx][tmpIdy];
                  if (piece === player) {
                      oneDPos = ((x + 1) * 8 + (y - 1));
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x + 1, y - 1]);
                      break
                  }
                  else {
                      tmpPieces.push([tmpIdx, tmpIdy])
                  }

                  tmpIdx--;
                  tmpIdy++;
              }
          }
          //diagonal top right
          if (x !== 0 && y !== 7 && x !== 7 && y !== 0 && boardTwoD[x - 1][y + 1] === 0) {
              tmpPieces = [[x, y]];
              tmpIdx = x + 1;
              tmpIdy = y - 1;
              piece = boardTwoD[tmpIdx][tmpIdy];
              while (tmpIdx !== 8 && tmpIdy !== -1 && piece !== 0) {
                  piece = boardTwoD[tmpIdx][tmpIdy];
                  if (piece === player) {
                      oneDPos = ((x - 1) * 8 + (y + 1));
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x - 1, y + 1]);
                      break
                  }
                  else {
                      tmpPieces.push([tmpIdx, tmpIdy])
                  }

                  tmpIdx++;
                  tmpIdy--;
              }
          }
          //diagonal bottom right
          if (x !== 7 && y !== 7 && x !== 0 && y !== 0 && boardTwoD[x + 1][y + 1] === 0) {
              tmpPieces = [[x, y]];
              tmpIdx = x - 1;
              tmpIdy = y - 1;
              piece = boardTwoD[tmpIdx][tmpIdy];
              while (tmpIdx !== -1 && tmpIdy !== -1 && piece !== 0) {
                  piece = boardTwoD[tmpIdx][tmpIdy];
                  if (piece === player) {
                      oneDPos = ((x + 1) * 8 + (y + 1));
                      if(map.has(oneDPos)){
                          let tmpValue = map.get(oneDPos);
                          tmpPieces = tmpValue.concat(tmpPieces)
                      }
                      map.set(oneDPos, tmpPieces);
                      moves.push([x + 1, y + 1]);
                      break
                  }
                  else {
                      tmpPieces.push([tmpIdx, tmpIdy])
                  }

                  tmpIdx--;
                  tmpIdy--;
              }
          }

      }
      return [moves, map];
  }

  getPieces(boardTwoD, player){
      let indices = [];
      for (let x = 0; x < 8; x ++){
          let idx = boardTwoD[x].indexOf(player);
          while( idx !== -1){
                indices.push([x, idx]);
                idx = boardTwoD[x].indexOf(player, idx + 1);
          }
      }
      return indices;
  }

  moveOneD(move){
      let moveOneD;
      moveOneD = move[0] * 8 + move[1];
      return moveOneD
  }

  updateBoard(board, move, piecesChange, player){
        let oldBoardTwoD = [];
        for (let i=0; i < board.length; i++)
            oldBoardTwoD[i] = board[i].slice();

        oldBoardTwoD[move[0]][move[1]] = player;
        for (let i = 0; i < piecesChange.length ; i ++){
            oldBoardTwoD[piecesChange[i][0]][piecesChange[i][1]] = player;
        }
        return oldBoardTwoD;
  }

  evaluation(board, player, enemyPlayer){

      return (10 * this.pieceDifference(board, player, enemyPlayer) +
          //801.724
          //801.724 * this.cornerOccupancy(board, player, enemyPlayer) +
          //382.026
          382.026 * this.cornerCloseness(board, player, enemyPlayer) +
          78.922 * this.mobility(board, player, enemyPlayer)+
          10 * this.discSquares(board, player, enemyPlayer));
  }

  countPieces (array, player){
      return array.filter(function (value) {
          return value === player;
      }).length
  }

  pieceDifference(board, player, enemyPlayer){
      let playerOne = 0; //player
      let playerTwo = 0; //enemy
      for (let i = 0; i < board.length; i ++){
            playerOne += this.countPieces(board[i], player);
            playerTwo += this.countPieces(board[i], enemyPlayer);
      }
      if(playerOne > playerTwo){
        return 100 * ( playerOne / (playerOne + playerTwo) )
      }
      else if(playerOne < playerTwo){
        return -100 * (playerTwo / (playerOne + playerTwo))
      }
      else{
        return 0
      }
  }

  cornerOccupancy(board, player, enemyPlayer){
      let playerCorner = 0;
      let enemyCorner = 0;
      let corners = [board[0][0], board[7][7], board[0][7], board[7][0]];
      playerCorner = this.countPieces(corners, player);
      enemyCorner = this.countPieces(corners, enemyPlayer);

      return playerCorner * 25 - enemyCorner * 25;

  }

  cornerCloseness(board,  player, enemyPlayer){
      let playerCCloseness = 0;
      let enemyCCloseness = 0;
      let corners = [board[0][0], board[7][7], board[0][7], board[7][0]];
      let adjCSpace = [[board[0][1],board[1][0], board[1][1]],[board[7][6], board[6][7], board[6][6]],[board[0][6],
          board[1][7], board[1][6]],[board[6][0], board[7][1], board[6][1]]];
      for (let i = 0; i < corners.length ; i ++){
          if (corners[i] === 0){
              playerCCloseness += this.countPieces(adjCSpace[i], player);
              enemyCCloseness += this.countPieces(adjCSpace[i], enemyPlayer);
          }
      }

      return -playerCCloseness * 12.5 + enemyCCloseness * 12.5

  }

  mobility(board, player, enemyPlayer){
      let indices = this.getPieces(board, enemyPlayer);
      let indicesEnemy = this.getPieces(board, player);

      let playerMoves = this.getLegalMoves(board, indices, player)[0].length;
      let enemyMoves = this.getLegalMoves(board, indicesEnemy, enemyPlayer)[0].length;

      if(playerMoves > enemyMoves){
          return 100 * (playerMoves / (playerMoves + enemyMoves))
      }
      else if(enemyMoves > playerMoves){
          return -100 * (enemyMoves / (playerMoves + enemyMoves))
      }
      else{
          return 0;
      }

  }

  discSquares(board, player, enemyPlayer){
      let boardValues = [[20, -3 , 11, 8,8,11,-3,20],[-3, -7, -4,1,1,4,-7,-3],[11, -4, 2, 2,2,2,-4,11],
          [8, 1, 2, -3,-3,2,1,8], [8, 1, 2, -3,-3,2,1,8], [11, -4, 2, 2,2,2,-4,11], [-3, -7, -4,1,1,4,-7,-3],
          [20, -3 , 11, 8,8,11,-3,20] ];
      let value = 0;
      for (let i = 0; i < board.length; i++){
          for (let j = 0; j < board.length; j ++){
              let piece = board[i][j];
              if (piece === enemyPlayer)
                  piece = -1;
              value += boardValues[i][j] * piece;
          }
      }

      return value;
  }

  frontierDiscs(board, player, enemyPlayer){
      let whitePcs = this.getPieces(board,0);

  }

  //sort highest first
  moveOrdering(legalmoves){
      let boardValues = [[20, -3 , 11, 8,8,11,-3,20],[-3, -7, -4,1,1,4,-7,-3],[11, -4, 2, 2,2,2,-4,11],
          [8, 1, 2, -3,-3,2,1,8], [8, 1, 2, -3,-3,2,1,8], [11, -4, 2, 2,2,2,-4,11], [-3, -7, -4,1,1,4,-7,-3],
          [20, -3 , 11, 8,8,11,-3,20] ];
      legalmoves.sort(
          function(a,b){
              if(boardValues[a[0]][a[1]] > boardValues[b[0]][b[1]]){
                  return -1
              }
              else if(boardValues[a[0]][a[1]] < boardValues[b[0]][b[1]]){
                  return 1
              }
              else {
                  return 0
              }
          });
      return legalmoves;
  }

  //iterative deepening
  minMaxManager (board,player, enemyPlayer, startTime, currTime){

      let move;
      let transpositionTable = new Map();
      let plyrPcs = this.getPieces(board, player);
      let enmyPlyrPcs = this.getPieces(board, enemyPlayer);
      let minMax;
      if (Math.abs(plyrPcs.length - enmyPlyrPcs.length) >= 50){
          minMax = this.minMax(board, 1,player, enemyPlayer, 14, 0, Infinity, -Infinity, transpositionTable);
      }
      else {
          minMax = this.minMax(board, 1, player, enemyPlayer, 6, 0, Infinity, -Infinity, transpositionTable);
      }
      move = minMax[1];

      return move
  }



  minMax(board, max,player, enemyPlayer ,depth, currDepth, beta, alpha, transpositionTable){
      let thisDepth = currDepth;
      let plyrPcs = this.getPieces(board, player);
      let enmyPlyrPcs = this.getPieces(board, enemyPlayer);
      let corners = [0, 7, 56, 63];

      //end-game solver


      let move;
      let value;
      let legalMoves = this.getLegalMoves(board, this.getPieces(board, enemyPlayer), player);
      let moves = legalMoves[0];
      moves = this.moveOrdering(moves);

      if (max === 1) {
          if (Math.abs(plyrPcs.length - enmyPlyrPcs.length) >= 50  || (plyrPcs.length + enmyPlyrPcs.length) === 64 ){
              return [this.pieceDifference(board, player, enemyPlayer) * 10000]
          }
          if (depth === thisDepth){
              let stringBoard = board.toString();
              // transpositionTable
              if(transpositionTable.has(stringBoard)){
                  return [transpositionTable.get(stringBoard)];
              }
              let evalue = this.evaluation(board, player, enemyPlayer);
              transpositionTable.set(stringBoard, evalue);
              return [evalue];
          }
          thisDepth = currDepth + 1 ;
          let max = -Infinity;
          if(legalMoves[1].has(0)) {
              move = [0, 0];
              return [Infinity, move]
          }
          else if(legalMoves[1].has(7)){
              move = [0,7];
              return [Infinity, move]
          }
          else if(legalMoves[1].has(56)){
              move = [7,0];
              return [Infinity, move]
          }
          else if(legalMoves[1].has(63)){
              move = [7,7];
              return [Infinity, move]
          }

          for (let i = 0; i < moves.length; i++) {

              //killer move: corners


              let tmpMoveOneD = this.moveOneD(moves[i]);
              let tmpBoard = this.updateBoard(board, moves[i], legalMoves[1].get(tmpMoveOneD), player);
              value = this.minMax(tmpBoard, 2, enemyPlayer, player, depth, thisDepth, beta, alpha, transpositionTable)[0];

              if (value > max) {
                  alpha = value;
                  max = value;
                  move = moves[i];
              }

              if (alpha >= beta){
                  return [max, move]
              }
          }


          if(move === undefined){
              move = moves[0];
          }

          return [max, move]
      }
      else {
          if (Math.abs(plyrPcs.length - enmyPlyrPcs.length) >= 50  || (plyrPcs.length + enmyPlyrPcs.length) === 64 ){
              return [this.pieceDifference(board, player, enemyPlayer) * -10000]
          }
          if (depth === thisDepth){
              let stringBoard = board.toString();
              // transpositionTable
              if(transpositionTable.has(stringBoard)){
                  return [transpositionTable.get(stringBoard)];
              }
              let evalue = -this.evaluation(board, player, enemyPlayer);
              transpositionTable.set(stringBoard, evalue);
              return [evalue];
          }
          thisDepth = currDepth + 1 ;
          let min = Infinity;
          if(legalMoves[1].has(0)) {
              move = [0, 0];
              return [-Infinity, move]
          }
          else if(legalMoves[1].has(7)){
              move = [0,7];
              return [-Infinity, move]
          }
          else if(legalMoves[1].has(56)){
              move = [7,0];
              return [-Infinity, move]
          }
          else if(legalMoves[1].has(63)){
              move = [7,7];
              return [-Infinity, move]
          }
          for (let i = 0; i < moves.length; i++) {

              let tmpMoveOneD = this.moveOneD(moves[i]);
              let tmpBoard = this.updateBoard(board, moves[i], legalMoves[1].get(tmpMoveOneD), player);
              value = this.minMax(tmpBoard, 1, enemyPlayer, player, depth, thisDepth, beta, alpha, transpositionTable)[0];

              if (value < min) {
                  beta = value;
                  min = value;
                  move = moves[i];
              }
              if(alpha >= beta){
                  return [min, move];
              }
          }

          if(move === undefined){
              move = moves[0];
          }

          return [min, move];
      }
  }

  ready = (data) => {
      //console.log("ready");
      let gameID = data.game_id;
      let playerTurnID = data.player_turn_id;
      let board = data.board;
      let startTime = new Date();

  //    console.log(gameID);
   //   console.log("id del juego");

      console.log(playerTurnID);
      console.log("^turno de jugador");

      //console.log(board);
      //console.log("^tablero");

      let boardTwoD = [];
      while(board.length)
        boardTwoD.push(board.splice(0, 8));

      let enemyPlayer = 1;
      if (playerTurnID === 1){
          enemyPlayer = 2;
      }

      let currTime = new Date();
      let move = this.minMaxManager(boardTwoD, playerTurnID, enemyPlayer, startTime, currTime);

      let moveOneD = this.moveOneD(move);

      this.state.socket.emit('play', {
          tournament_id: this.state.tournament_id,
          player_turn_id: playerTurnID,
          game_id: gameID,
          movement: moveOneD
      });
  };

  finish = (data) => {
      console.log("finish");
      let gameID = data.game_id;
      let playerTurnID = data.player_turn_id;
      let winnerTurnID = data.winner_turn_id;
      let board = data.board;

      // TODO: Your cleaning board logic here

      this.state.socket.emit('player_ready', {
          tournament_id: this.state.tournament_id,
          player_turn_id: playerTurnID,
          game_id: gameID
      });
  };

  render() {

    /* this.state.socket.on('connect',  () => {
        this.signin();
    });
    */
      this.state.socket.on('ok_signin', () => {
        console.log("Successfully signed in!");
    });

      this.state.socket.on('ready', (data) => {
        this.ready(data)
    });

      this.state.socket.on('finish', (data) => {
        this.finish( data)
     });

    return (
      <div>
          Username : <input name="username" value={this.state.username} onChange={this.handleNameChange}/>
          <div/>
          tournament ID: <input type="number" name="tournamenr_id" value={this.state.tournament_id} onChange={this.handleTnmtChange} />
          <div/>
          IP : <input name="ip" value={this.state.endpoint} onChange={this.handleIpChange}/>
          <div/>
          <button onClick={() => this.signin(this.state.socket)}>Connect me now!</button>
      </div>
    );
  }
}

export default App;
